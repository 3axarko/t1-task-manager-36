package ru.t1.zkovalenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.endpoint.IProjectEndpoint;
import ru.t1.zkovalenko.tm.api.endpoint.ITaskEndpoint;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.zkovalenko.tm.dto.request.task.*;
import ru.t1.zkovalenko.tm.enumerated.Sort;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.marker.IntegrationCategory;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.service.PropertyService;

import java.util.List;

import static ru.t1.zkovalenko.tm.constant.EndpointTestData.USER_ADMIN_LOGIN;
import static ru.t1.zkovalenko.tm.constant.EndpointTestData.USER_ADMIN_PASSWORD;
import static ru.t1.zkovalenko.tm.enumerated.Status.*;

@Category(IntegrationCategory.class)
public class TaskEndpointTest extends AbstractEndpointTest {

    @Nullable
    private String userToken;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Nullable
    private Task testTask;

    @NotNull
    private Task testTaskCreate() {
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(userToken);
        createRequest.setName("TASK_NAME");
        createRequest.setDescription("Description");
        @Nullable final Task taskCreated = taskEndpoint.createTask(createRequest).getTask();
        Assert.assertNotNull(taskCreated);
        return taskCreated;
    }

    @Nullable
    private Project testProjectCreate() {
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken);
        createRequest.setName("PROJECT_NAME");
        createRequest.setDescription("Description");
        @Nullable final Project projectCreated = projectEndpoint.createProject(createRequest).getProject();
        return projectCreated;
    }

    @Nullable
    private Task taskFindById(final String Id) {
        @NotNull final TaskShowByIdRequest showByIdRequest = new TaskShowByIdRequest(userToken);
        showByIdRequest.setTaskId(Id);
        @Nullable final Task taskFounded = taskEndpoint.showByIdTask(showByIdRequest).getTask();
        return taskFounded;
    }

    @Nullable
    private Task taskFindByIndex(final Integer Index) {
        @NotNull final TaskShowByIndexRequest showByIndexRequest = new TaskShowByIndexRequest(userToken);
        showByIndexRequest.setIndex(Index);
        @Nullable final Task taskFounded = taskEndpoint.showByIndexTask(showByIndexRequest).getTask();
        return taskFounded;
    }

    @Before
    public void before() {
        userToken = login(USER_ADMIN_LOGIN, USER_ADMIN_PASSWORD);
        saveBackup(userToken);
        serverAutoBackup(false, userToken);
        testTask = testTaskCreate();
    }

    @After
    public void after() {
        loadBackup(userToken);
        serverAutoBackup(true, userToken);
        logout(userToken);
    }

    @Test
    public void taskChangeStatusById() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(userToken);
        request.setTaskId(testTask.getId());
        request.setStatus(COMPLETED);
        taskEndpoint.changeStatusByIdTask(request);
        @Nullable Task taskFounded = taskFindById(testTask.getId());
        Assert.assertEquals(taskFounded.getStatus(), COMPLETED);
    }

    @Test
    public void taskChangeStatusByIndex() {
        @NotNull final Status statusBefore = taskFindByIndex(0).getStatus();
        @NotNull final Status statusToSet = statusBefore == COMPLETED ? NOT_STARTED : COMPLETED;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(userToken);
        request.setIndex(0);
        request.setStatus(statusToSet);
        taskEndpoint.changeStatusByIndexTask(request);
        @Nullable Task taskFounded = taskFindByIndex(0);
        Assert.assertEquals(taskFounded.getStatus(), statusToSet);
    }

    @Test
    public void clearTask() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        taskEndpoint.clearTask(request);
        Assert.assertNull(taskFindById(testTask.getId()));
    }

    @Test
    public void taskCompleteById() {
        @NotNull final TaskChangeStatusByIdRequest requestChange = new TaskChangeStatusByIdRequest(userToken);
        requestChange.setTaskId(testTask.getId());
        requestChange.setStatus(IN_PROGRESS);
        taskEndpoint.changeStatusByIdTask(requestChange);
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(userToken);
        request.setTaskId(testTask.getId());
        taskEndpoint.completeByIdTask(request);
        @Nullable final Task taskFounded = taskFindById(testTask.getId());
        Assert.assertEquals(taskFounded.getStatus(), COMPLETED);
    }

    @Test
    public void taskCompleteByIndex() {
        @NotNull final TaskChangeStatusByIndexRequest requestChange = new TaskChangeStatusByIndexRequest(userToken);
        requestChange.setIndex(0);
        requestChange.setStatus(IN_PROGRESS);
        taskEndpoint.changeStatusByIndexTask(requestChange);
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(userToken);
        request.setIndex(0);
        taskEndpoint.completeByIndexTask(request);
        @Nullable final Task taskFounded = taskFindByIndex(0);
        Assert.assertEquals(taskFounded.getStatus(), COMPLETED);
    }

    @Test
    public void listTask() {
        @NotNull final TaskListRequest request = new TaskListRequest(userToken);
        request.setSort(Sort.BY_NAME);
        @Nullable final List<Task> taskList = taskEndpoint.listTask(request).getTasks();
        Assert.assertTrue(taskList.size() >= 1);
    }

    @Test
    public void removeByIdTask() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(userToken);
        request.setTaskId(testTask.getId());
        taskEndpoint.removeByIdTask(request);
        Assert.assertNull(taskFindById(testTask.getId()));
    }

    @Test
    public void removeByIndexTask() {
        @NotNull final Task taskFound = taskFindByIndex(0);
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(userToken);
        request.setIndex(0);
        taskEndpoint.removeByIndexTask(request);
        Assert.assertNull(taskFindById(taskFound.getId()));
    }

    @Test
    public void taskStartById() {
        @NotNull final TaskChangeStatusByIdRequest requestChange = new TaskChangeStatusByIdRequest(userToken);
        requestChange.setTaskId(testTask.getId());
        requestChange.setStatus(NOT_STARTED);
        taskEndpoint.changeStatusByIdTask(requestChange);
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(userToken);
        request.setTaskId(testTask.getId());
        taskEndpoint.startByIdTask(request);
        @Nullable final Task taskFounded = taskFindById(testTask.getId());
        Assert.assertEquals(taskFounded.getStatus(), IN_PROGRESS);
    }

    @Test
    public void taskStartByIndex() {
        @NotNull final TaskChangeStatusByIndexRequest requestChange = new TaskChangeStatusByIndexRequest(userToken);
        requestChange.setIndex(0);
        requestChange.setStatus(NOT_STARTED);
        taskEndpoint.changeStatusByIndexTask(requestChange);
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(userToken);
        request.setIndex(0);
        taskEndpoint.startByIndexTask(request);
        @Nullable final Task taskFounded = taskFindByIndex(0);
        Assert.assertEquals(taskFounded.getStatus(), IN_PROGRESS);
    }

    @Test
    public void taskUpdateById() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(userToken);
        request.setTaskId(testTask.getId());
        request.setName(testTask.getName() + "1");
        request.setDescription(testTask.getDescription() + "1");
        taskEndpoint.updateByIdTask(request);
        @Nullable final Task taskFounded = taskFindById(testTask.getId());
        Assert.assertEquals(taskFounded.getName(), testTask.getName() + "1");
        Assert.assertEquals(taskFounded.getDescription(), testTask.getDescription() + "1");
    }

    @Test
    public void taskUpdateByIndex() {
        @Nullable final Task taskToChange = taskFindByIndex(0);
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(userToken);
        request.setIndex(0);
        request.setName(taskToChange.getName() + "1");
        request.setDescription(taskToChange.getDescription() + "1");
        taskEndpoint.updateByIndexTask(request);
        @Nullable final Task taskFounded = taskFindById(taskToChange.getId());
        Assert.assertEquals(taskFounded.getName(), taskToChange.getName() + "1");
        Assert.assertEquals(taskFounded.getDescription(), taskToChange.getDescription() + "1");
    }

    @Test
    public void taskOperationsWithProject() {
        @NotNull final Project testProject = testProjectCreate();
        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(userToken);
        requestBind.setTaskId(testTask.getId());
        requestBind.setProjectId(testProject.getId());
        taskEndpoint.bindToProjectTask(requestBind);

        @NotNull final TaskShowByProjectIdRequest requestShow = new TaskShowByProjectIdRequest(userToken);
        requestShow.setProjectId(testProject.getId());
        @Nullable final List<Task> tasksFounded = taskEndpoint.showByProjectIdTask(requestShow).getTasks();
        Assert.assertEquals(0, tasksFounded
                .stream()
                .filter(task -> task.getId() == testTask.getId())
                .count());

        @NotNull final TaskUnbindFromProjectRequest requestUnbind = new TaskUnbindFromProjectRequest(userToken);
        requestUnbind.setProjectId(testProject.getId());
        requestUnbind.setTaskId(testTask.getId());
        taskEndpoint.unbindFromProjectTask(requestUnbind);
        @NotNull final Task taskUnbind = taskFindById(testTask.getId());
        Assert.assertNull(taskUnbind.getProjectId());
    }

}
