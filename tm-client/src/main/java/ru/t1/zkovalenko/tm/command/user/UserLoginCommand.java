package ru.t1.zkovalenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.UserLoginRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "login";

    @NotNull
    private final String DESCRIPTION = "User login";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        @Nullable final String token = getAuthEndpoint().login(request).getToken();
        setToken(token);
//        if (token != null) System.out.println("[Logged In]");
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
