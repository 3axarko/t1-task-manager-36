package ru.t1.zkovalenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-update-profile";

    @NotNull
    private final String DESCRIPTION = "User update profile";

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("First name");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Last name");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Middle name");
        @NotNull final String middleName = TerminalUtil.nextLine();
        UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        getUserEndpoint().updateProfileUser(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
