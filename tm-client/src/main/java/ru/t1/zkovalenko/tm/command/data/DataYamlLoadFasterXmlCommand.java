package ru.t1.zkovalenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.data.DataYamlLoadFasterXmlRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;

public class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml-fasterxml";

    @NotNull
    public static final String ARGUMENT = "-dlyf";

    @NotNull
    public static final String DESCRIPTION = "Load data Yaml FasterXML from file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA YAML FASTERXML LOAD]");
        @NotNull DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(getToken());
        getDomainEndpoint().yamlLoadFasterXmlData(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
