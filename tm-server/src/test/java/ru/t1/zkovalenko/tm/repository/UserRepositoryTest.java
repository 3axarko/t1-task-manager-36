package ru.t1.zkovalenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.repository.IUserRepository;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.marker.UnitCategory;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.service.PropertyService;
import ru.t1.zkovalenko.tm.util.HashUtil;

import static ru.t1.zkovalenko.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Test
    public void add() {
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findOneById(USER1.getId()));
    }

    @Test
    public void addCollection() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER_LIST);
        Assert.assertEquals(USER_LIST, repository.findAll());
    }

    @Test
    public void remove() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final User user1 = repository.add(USER1);
        @NotNull final User user2 = repository.add(USER2);
        repository.remove(user1);
        Assert.assertNotEquals(user2, repository.findOneById(USER1.getId()));
        Assert.assertEquals(user2, repository.findOneById(USER2.getId()));
    }

    @Test
    public void clearAndGetSize() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        repository.add(USER2);
        Assert.assertEquals(2, repository.getSize());
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findAll() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final User user = repository.add(USER1);
        Assert.assertEquals(user, repository.findAll().get(0));
    }

    @Test
    public void existById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final User user = repository.add(USER1);
        Assert.assertTrue(repository.existById(user.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final User user = repository.add(USER1);
        Assert.assertEquals(user, repository.findOneById(USER1.getId()));
    }

    @Test
    public void findOneByIndex() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final User user = repository.add(USER1);
        Assert.assertEquals(user, repository.findOneByIndex(0));
    }

    @Test
    public void removeById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final User user1 = repository.add(USER1);
        @NotNull final User user2 = repository.add(USER2);
        repository.removeById(user2.getId());
        Assert.assertEquals(user1, repository.findOneById(USER1.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final User user1 = repository.add(USER1);
        repository.add(USER2);
        repository.removeByIndex(1);
        Assert.assertEquals(user1, repository.findOneById(USER1.getId()));
        Assert.assertFalse(repository.existById(USER2.getId()));
    }

    /* User Repository */

    @Test
    public void createLoginPassword() {
        @Nullable final User userCreated = repository.create(propertyService, USER1.getLogin(), USER_PASSWORD);
        @Nullable final User userFunded = repository.findOneById(userCreated.getId());
        Assert.assertEquals(userFunded.getPasswordHash(), HashUtil.salt(propertyService, USER_PASSWORD));
        Assert.assertEquals(userFunded.getLogin(), USER1.getLogin());
    }

    @Test
    public void createLoginPasswordEmail() {
        @Nullable final User userCreated = repository.create(propertyService,
                USER1.getLogin(),
                USER_PASSWORD,
                USER1.getEmail());
        @Nullable final User userFunded = repository.findOneById(userCreated.getId());
        Assert.assertEquals(userFunded.getPasswordHash(), HashUtil.salt(propertyService, USER_PASSWORD));
        Assert.assertEquals(userFunded.getLogin(), USER1.getLogin());
        Assert.assertEquals(userFunded.getEmail(), USER1.getEmail());
    }

}
