package ru.t1.zkovalenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.api.repository.IUserRepository;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.api.service.IUserService;
import ru.t1.zkovalenko.tm.exception.user.UserNotFoundException;
import ru.t1.zkovalenko.tm.marker.UnitCategory;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.repository.ProjectRepository;
import ru.t1.zkovalenko.tm.repository.TaskRepository;
import ru.t1.zkovalenko.tm.repository.UserRepository;
import ru.t1.zkovalenko.tm.util.HashUtil;

import static ru.t1.zkovalenko.tm.constant.UserTestData.*;
import static ru.t1.zkovalenko.tm.enumerated.Role.ADMIN;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(
            propertyService,
            userRepository,
            projectRepository,
            taskRepository
    );

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void createByLoginPassword() {
        @Nullable final User userCreated = userService.create(USER1.getLogin(), USER_PASSWORD);
        @Nullable final User userFunded = userService.findOneById(userCreated.getId());
        Assert.assertEquals(userFunded.getPasswordHash(), HashUtil.salt(propertyService, USER_PASSWORD));
        Assert.assertEquals(userFunded.getLogin(), USER1.getLogin());
    }

    @Test
    public void createByLoginPasswordEmail() {
        @Nullable final User userCreated = userService.create(
                USER1.getLogin(),
                USER_PASSWORD,
                USER1.getEmail());
        @Nullable final User userFunded = userService.findOneById(userCreated.getId());
        Assert.assertEquals(userFunded.getPasswordHash(), HashUtil.salt(propertyService, USER_PASSWORD));
        Assert.assertEquals(userFunded.getLogin(), USER1.getLogin());
        Assert.assertEquals(userFunded.getEmail(), USER1.getEmail());
    }

    @Test
    public void createByLoginPasswordRule() {
        @Nullable final User userCreated = userService.create(
                USER1.getLogin(),
                USER_PASSWORD,
                ADMIN);
        @Nullable final User userFunded = userService.findOneById(userCreated.getId());
        Assert.assertEquals(userFunded.getPasswordHash(), HashUtil.salt(propertyService, USER_PASSWORD));
        Assert.assertEquals(userFunded.getLogin(), USER1.getLogin());
        Assert.assertEquals(userFunded.getRole(), ADMIN);
    }

    @Test
    public void findByLogin() {
        @Nullable final User userCreated = userService.create(USER1.getLogin(), USER_PASSWORD);
        Assert.assertEquals(userCreated, userService.findByLogin(USER1.getLogin()));
    }

    @Test
    public void findByEmail() {
        @Nullable final User userCreated = userService.create(
                USER1.getLogin(),
                USER_PASSWORD,
                USER1.getEmail());
        Assert.assertEquals(userCreated, userService.findByEmail(USER1.getEmail()));
    }

    @Test
    public void removeByLogin() {
        userService.create(USER1.getLogin(), USER_PASSWORD);
        userService.findByLogin(USER1.getLogin());
        userService.removeByLogin(USER1.getLogin());
        thrown.expect(UserNotFoundException.class);
        userService.findByLogin(USER1.getLogin());
    }

    @Test
    public void removeByEmail() {
        userService.create(USER1.getLogin(), USER_PASSWORD, USER1.getEmail());
        userService.findByEmail(USER1.getEmail());
        userService.removeByLogin(USER1.getLogin());
        thrown.expect(UserNotFoundException.class);
        userService.findByLogin(USER1.getLogin());
    }

    @Test
    public void setPassword() {
        @NotNull final User userCreated = userService.create(USER1.getLogin(), USER_PASSWORD);
        userService.setPassword(userCreated.getLogin(), USER_PASSWORD + "1");
        @Nullable final User userFounded = userService.findOneById(userCreated.getId());
        Assert.assertEquals(userFounded.getPasswordHash(), HashUtil.salt(propertyService, USER_PASSWORD + "1"));
    }

    @Test
    public void updateUser() {
        @NotNull final User userCreated = userService.create(USER1.getLogin(), USER_PASSWORD);
        userService.updateUser(
                userCreated.getId(),
                TEST_FIO,
                TEST_FIO,
                TEST_FIO);
        @Nullable final User userFounded = userService.findOneById(userCreated.getId());
        Assert.assertEquals(userFounded.getFirstName(), TEST_FIO);
        Assert.assertEquals(userFounded.getLastName(), TEST_FIO);
        Assert.assertEquals(userFounded.getMiddleName(), TEST_FIO);
    }

    @Test
    public void isLoginExist() {
        userService.create(USER1.getLogin(), USER_PASSWORD);
        Assert.assertTrue(userService.isLoginExist(USER1.getLogin()));
    }

    @Test
    public void isEmailExist() {
        userService.create(USER1.getLogin(), USER_PASSWORD, USER1.getEmail());
        Assert.assertTrue(userService.isEmailExist(USER1.getEmail()));
    }

    @Test
    public void lockUnlockUserByLogin() {
        userService.create(USER1.getLogin(), USER_PASSWORD);
        userService.lockUserByLogin(USER1.getLogin());
        @Nullable User userFounded = userService.findByLogin(USER1.getLogin());
        Assert.assertTrue(userFounded.getLocked());
        userService.unlockUserByLogin(USER1.getLogin());
        userFounded = userService.findByLogin(USER1.getLogin());
        Assert.assertFalse(userFounded.getLocked());
    }

}
