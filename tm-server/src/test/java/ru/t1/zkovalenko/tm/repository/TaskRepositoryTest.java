package ru.t1.zkovalenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.marker.UnitCategory;
import ru.t1.zkovalenko.tm.model.Task;

import static ru.t1.zkovalenko.tm.constant.TaskTestData.TASK1;
import static ru.t1.zkovalenko.tm.constant.TaskTestData.TASK2;
import static ru.t1.zkovalenko.tm.constant.UserTestData.USER1;
import static ru.t1.zkovalenko.tm.constant.UserTestData.USER2;
import static ru.t1.zkovalenko.tm.enumerated.Sort.BY_NAME;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    /* AbstractUserOwnerRepository */

    @Test
    public void add() {
        @Nullable final Task task = repository.add(USER1.getId(), TASK1);
        Assert.assertEquals(task, repository.findOneById(USER1.getId(), task.getId()));
    }

    @Test
    public void remove() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task1 = repository.create(USER1.getId(), TASK1.getName());
        @NotNull final Task task2 = repository.create(USER2.getId(), TASK2.getName());
        repository.remove(USER1.getId(), task1);
        Assert.assertNotEquals(task2, repository.findOneById(USER1.getId(), task2.getId()));
        Assert.assertEquals(task2, repository.findOneById(USER2.getId(), task2.getId()));
    }

    @Test
    public void clearAndGetSize() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.create(USER1.getId(), TASK1.getName());
        repository.create(USER1.getId(), TASK2.getName());
        Assert.assertEquals(2, repository.getSize(USER1.getId()));
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findAllWithOneParam() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = repository.create(USER1.getId(), TASK1.getName());
        Assert.assertEquals(task, repository.findAll(USER1.getId()).get(0));
    }

    @Test
    public void findAllWithTwoParams() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = repository.create(USER1.getId(), TASK1.getName());
        Assert.assertEquals(task, repository.findAll(USER1.getId(), BY_NAME.getComparator()).get(0));
    }

    @Test
    public void existById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = repository.create(USER1.getId(), TASK1.getName());
        Assert.assertTrue(repository.existById(task.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = repository.create(USER1.getId(), TASK1.getName());
        Assert.assertEquals(task, repository.findOneById(USER1.getId(), task.getId()));
    }

    @Test
    public void findOneByIndex() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task = repository.create(USER1.getId(), TASK1.getName());
        Assert.assertEquals(task, repository.findOneByIndex(USER1.getId(), 0));
    }

    @Test
    public void removeById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task1 = repository.create(USER1.getId(), TASK1.getName());
        @NotNull final Task task2 = repository.create(USER1.getId(), TASK2.getName());
        repository.removeById(task2.getId());
        Assert.assertEquals(task1, repository.findAll(USER1.getId()).get(0));
    }

    @Test
    public void removeByIndex() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Task task1 = repository.create(USER1.getId(), TASK1.getName());
        repository.create(USER1.getId(), TASK2.getName());
        repository.removeByIndex(1);
        Assert.assertEquals(task1, repository.findAll(USER1.getId()).get(0));
    }

}
