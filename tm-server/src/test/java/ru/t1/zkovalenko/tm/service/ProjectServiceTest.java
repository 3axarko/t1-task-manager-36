package ru.t1.zkovalenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.api.service.IProjectService;
import ru.t1.zkovalenko.tm.marker.UnitCategory;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.repository.ProjectRepository;

import static ru.t1.zkovalenko.tm.constant.ProjectTestData.*;
import static ru.t1.zkovalenko.tm.constant.UserTestData.USER1;
import static ru.t1.zkovalenko.tm.enumerated.Status.COMPLETED;
import static ru.t1.zkovalenko.tm.enumerated.Status.NOT_STARTED;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Test
    public void changeProjectStatusById() {
        @NotNull final Project projectCreated = projectService.create(USER1.getId(), PROJECT1.getId());
        Assert.assertEquals(projectCreated.getStatus(), NOT_STARTED);
        @NotNull final Project projectChanged = projectService
                .changeProjectStatusById(USER1.getId(), projectCreated.getId(), COMPLETED);
        Assert.assertEquals(projectChanged.getStatus(), COMPLETED);
    }

    @Test
    public void changeProjectStatusByIndex() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        @NotNull final Project projectCreated = projectService.create(USER1.getId(), PROJECT1.getId());
        Assert.assertEquals(projectCreated.getStatus(), NOT_STARTED);
        @NotNull final Project projectChanged = projectService
                .changeProjectStatusByIndex(USER1.getId(), 0, COMPLETED);
        Assert.assertEquals(projectChanged.getStatus(), COMPLETED);
    }

    @Test
    public void createUserIdName() {
        @NotNull final Project projectCreated = projectService.create(USER1.getId(), PROJECT_NAME);
        @NotNull final Project projectFounded = projectService.findOneById(projectCreated.getId());
        Assert.assertEquals(projectFounded, projectCreated);
        Assert.assertEquals(PROJECT_NAME, projectFounded.getName());
    }

    @Test
    public void createUserIdNameDescription() {
        @NotNull final Project projectCreated = projectService.create(USER1.getId(), PROJECT_NAME, PROJECT_DESCRIPTION);
        @NotNull final Project projectFounded = projectService.findOneById(projectCreated.getId());
        Assert.assertEquals(projectFounded, projectCreated);
        Assert.assertEquals(PROJECT_NAME, projectFounded.getName());
        Assert.assertEquals(PROJECT_DESCRIPTION, projectFounded.getDescription());
    }

    @Test
    public void updateById() {
        @NotNull final Project projectCreated = projectService.create(USER1.getId(), PROJECT_NAME, PROJECT_DESCRIPTION);
        projectService.updateById(USER1.getId(),
                projectCreated.getId(),
                PROJECT_NAME + "1",
                PROJECT_DESCRIPTION + "1");
        @NotNull final Project projectFounded = projectService.findOneById(projectCreated.getId());
        Assert.assertEquals(projectFounded.getName(), PROJECT_NAME + "1");
        Assert.assertEquals(projectFounded.getDescription(), PROJECT_DESCRIPTION + "1");
    }

    @Test
    public void updateByIndex() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        @NotNull final Project projectCreated = projectService.create(USER1.getId(), PROJECT_NAME, PROJECT_DESCRIPTION);
        projectService.updateByIndex(USER1.getId(),
                0,
                PROJECT_NAME + "1",
                PROJECT_DESCRIPTION + "1");
        @NotNull final Project projectFounded = projectService.findOneById(projectCreated.getId());
        Assert.assertEquals(projectFounded.getName(), PROJECT_NAME + "1");
        Assert.assertEquals(projectFounded.getDescription(), PROJECT_DESCRIPTION + "1");
    }

}
