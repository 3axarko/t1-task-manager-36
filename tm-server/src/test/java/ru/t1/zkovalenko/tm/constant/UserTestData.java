package ru.t1.zkovalenko.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.model.User;

import java.util.Arrays;
import java.util.List;

public final class UserTestData {

    @NotNull
    public final static User USER1 = new User();

    @NotNull
    public final static User USER2 = new User();

    @NotNull
    public final static String USER_LOGIN = "USER_LOGIN";

    @NotNull
    public final static String USER_PASSWORD = "USER_PASSWORD";

    @NotNull
    public final static String USER_EMAIL = "12@mail.ru";

    @NotNull
    public final static String TEST_FIO = "TEST";

    @NotNull
    public final static List<User> USER_LIST = Arrays.asList(USER1, USER2);

    static {
        for (Integer i = 0; i < USER_LIST.size(); i++) {
            USER_LIST.get(i).setLogin(USER_LOGIN + i);
            USER_LIST.get(i).setEmail(i + USER_EMAIL);
        }
    }

}
