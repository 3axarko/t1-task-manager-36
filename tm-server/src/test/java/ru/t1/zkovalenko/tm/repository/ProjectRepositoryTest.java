package ru.t1.zkovalenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.marker.UnitCategory;
import ru.t1.zkovalenko.tm.model.Project;

import static ru.t1.zkovalenko.tm.constant.ProjectTestData.*;
import static ru.t1.zkovalenko.tm.constant.UserTestData.USER1;
import static ru.t1.zkovalenko.tm.constant.UserTestData.USER2;
import static ru.t1.zkovalenko.tm.enumerated.Sort.BY_NAME;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @Test
    public void add() {
        @Nullable final Project project = repository.add(USER1.getId(), PROJECT1);
        Assert.assertEquals(project, repository.findOneById(USER1.getId(), project.getId()));
    }

    @Test
    public void remove() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project1 = repository.create(USER1.getId(), PROJECT1.getName());
        @NotNull final Project project2 = repository.create(USER2.getId(), PROJECT2.getName());
        repository.remove(USER1.getId(), project1);
        Assert.assertNotEquals(project2, repository.findOneById(USER1.getId(), project2.getId()));
        Assert.assertEquals(project2, repository.findOneById(USER2.getId(), project2.getId()));
    }

    @Test
    public void clearAndGetSize() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.create(USER1.getId(), PROJECT1.getName());
        repository.create(USER1.getId(), PROJECT2.getName());
        Assert.assertEquals(2, repository.getSize(USER1.getId()));
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findAllId() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = repository.create(USER1.getId(), PROJECT1.getName());
        Assert.assertEquals(project, repository.findAll(USER1.getId()).get(0));
    }

    @Test
    public void findAllIdName() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = repository.create(USER1.getId(), PROJECT1.getName());
        Assert.assertEquals(project, repository.findAll(USER1.getId(), BY_NAME.getComparator()).get(0));
    }

    @Test
    public void existById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = repository.create(USER1.getId(), PROJECT1.getName());
        Assert.assertTrue(repository.existById(project.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = repository.create(USER1.getId(), PROJECT1.getName());
        Assert.assertEquals(project, repository.findOneById(USER1.getId(), project.getId()));
    }

    @Test
    public void findOneByIndex() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project = repository.create(USER1.getId(), PROJECT1.getName());
        Assert.assertEquals(project, repository.findOneByIndex(USER1.getId(), 0));
    }

    @Test
    public void removeById() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project1 = repository.create(USER1.getId(), PROJECT1.getName());
        @NotNull final Project project2 = repository.create(USER1.getId(), PROJECT2.getName());
        repository.removeById(project2.getId());
        Assert.assertNull(repository.findOneById(project2.getId()));
        Assert.assertNotNull(repository.findOneById(project1.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Project project1 = repository.create(USER1.getId(), PROJECT1.getName());
        repository.create(USER1.getId(), PROJECT2.getName());
        repository.removeByIndex(USER1.getId(), 1);
        Assert.assertNull(repository.findOneByIndex(USER1.getId(), 1));
    }

    /* Project Repository */

    @Test
    public void createIdNameDescription() {
        @Nullable final Project projectCreated = repository.create(USER1.getId(), PROJECT_NAME, PROJECT_DESCRIPTION);
        Assert.assertNotNull(projectCreated);
        @Nullable final Project projectFunded = repository.findOneById(projectCreated.getId());
        Assert.assertEquals(projectFunded, projectCreated);
    }

    @Test
    public void createIdName() {
        @Nullable final Project projectCreated = repository.create(USER1.getId(), PROJECT_NAME);
        Assert.assertNotNull(projectCreated);
        @Nullable final Project projectFunded = repository.findOneById(projectCreated.getId());
        Assert.assertEquals(projectFunded, projectCreated);
    }

}
