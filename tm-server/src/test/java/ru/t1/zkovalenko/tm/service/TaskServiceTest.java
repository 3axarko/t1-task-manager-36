package ru.t1.zkovalenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.api.service.ITaskService;
import ru.t1.zkovalenko.tm.marker.UnitCategory;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.repository.TaskRepository;

import static ru.t1.zkovalenko.tm.constant.TaskTestData.*;
import static ru.t1.zkovalenko.tm.constant.UserTestData.USER1;
import static ru.t1.zkovalenko.tm.enumerated.Status.COMPLETED;
import static ru.t1.zkovalenko.tm.enumerated.Status.NOT_STARTED;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Test
    public void changeTaskStatusById() {
        @NotNull final Task taskCreated = taskService.create(USER1.getId(), TASK1.getId());
        Assert.assertEquals(taskCreated.getStatus(), NOT_STARTED);
        @NotNull final Task taskChanged = taskService
                .changeTaskStatusById(USER1.getId(), taskCreated.getId(), COMPLETED);
        Assert.assertEquals(taskChanged.getStatus(), COMPLETED);
    }

    @Test
    public void changeTaskStatusByIndex() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        @NotNull final Task taskCreated = taskService.create(USER1.getId(), TASK1.getId());
        Assert.assertEquals(taskCreated.getStatus(), NOT_STARTED);
        @NotNull final Task taskChanged = taskService
                .changeTaskStatusByIndex(USER1.getId(), 0, COMPLETED);
        Assert.assertEquals(taskChanged.getStatus(), COMPLETED);
    }

    @Test
    public void createUserIdName() {
        @NotNull final Task taskCreated = taskService.create(USER1.getId(), TASK_NAME);
        @NotNull final Task taskFounded = taskService.findOneById(taskCreated.getId());
        Assert.assertEquals(taskFounded, taskCreated);
        Assert.assertEquals(TASK_NAME, taskFounded.getName());
    }

    @Test
    public void createUserIdNameDescription() {
        @NotNull final Task taskCreated = taskService.create(USER1.getId(), TASK_NAME, TASK_DESCRIPTION);
        @NotNull final Task taskFounded = taskService.findOneById(taskCreated.getId());
        Assert.assertEquals(taskFounded, taskCreated);
        Assert.assertEquals(TASK_NAME, taskFounded.getName());
        Assert.assertEquals(TASK_DESCRIPTION, taskFounded.getDescription());
    }

    @Test
    public void updateById() {
        @NotNull final Task taskCreated = taskService.create(USER1.getId(), TASK_NAME, TASK_DESCRIPTION);
        taskService.updateById(USER1.getId(),
                taskCreated.getId(),
                TASK_NAME + "1",
                TASK_DESCRIPTION + "1");
        @NotNull final Task taskFounded = taskService.findOneById(taskCreated.getId());
        Assert.assertEquals(taskFounded.getName(), TASK_NAME + "1");
        Assert.assertEquals(taskFounded.getDescription(), TASK_DESCRIPTION + "1");
    }

    @Test
    public void updateByIndex() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        @NotNull final Task taskCreated = taskService.create(USER1.getId(), TASK_NAME, TASK_DESCRIPTION);
        taskService.updateByIndex(USER1.getId(),
                0,
                TASK_NAME + "1",
                TASK_DESCRIPTION + "1");
        @NotNull final Task taskFounded = taskService.findOneById(taskCreated.getId());
        Assert.assertEquals(taskFounded.getName(), TASK_NAME + "1");
        Assert.assertEquals(taskFounded.getDescription(), TASK_DESCRIPTION + "1");
    }

}
