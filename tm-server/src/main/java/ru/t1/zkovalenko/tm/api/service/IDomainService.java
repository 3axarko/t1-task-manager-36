package ru.t1.zkovalenko.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.Domain;

public interface IDomainService {

    @NotNull Domain getDomain();

    void setDomain(@Nullable Domain domain);

    @SneakyThrows
    void dataBackupLoad();

    @SneakyThrows
    void dataBackupSave();

    @SneakyThrows
    void dataBase64Load();

    @SneakyThrows
    void dataBase64Save();

    @SneakyThrows
    void dataBinaryLoad();

    @SneakyThrows
    void dataBinarySave();

    @SneakyThrows
    void dataJsonLoadJaxB();

    @SneakyThrows
    void dataJsonLoadJFasterXml();

    @SneakyThrows
    void dataJsonSaveFasterXml();

    @SneakyThrows
    void dataJsonSaveJaxB();

    @SneakyThrows
    void dataXmlLoadJaxB();

    @SneakyThrows
    void dataXmlLoadJFasterXml();

    @SneakyThrows
    void dataXmlSaveFasterXml();

    @SneakyThrows
    void dataXmlSaveJaxB();

    @SneakyThrows
    void dataYamlLoadFasterXml();

    @SneakyThrows
    void dataYamlSaveFasterXml();

    @SneakyThrows
    void dataDisableAutoBackup();

    @SneakyThrows
    void dataEnableAutoBackup();

}
