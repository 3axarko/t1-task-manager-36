package ru.t1.zkovalenko.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

@NoArgsConstructor
public class DataYamlLoadFasterXmlRequest extends AbstractUserRequest {

    public DataYamlLoadFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
