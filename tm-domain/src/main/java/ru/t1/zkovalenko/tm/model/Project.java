package ru.t1.zkovalenko.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.model.IWBS;
import ru.t1.zkovalenko.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnerModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    public Project(@NotNull String name) {
        this.name = name;
    }

    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public Project(@NotNull String name, @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
