package ru.t1.zkovalenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

public interface IConnectionProvider {

    @NotNull
    String getHost();

    @NotNull
    String getPort();

}
